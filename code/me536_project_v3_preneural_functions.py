# -*- coding: utf-8 -*-
"""ME536_Project_v3_PreNeural_Functions

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1Cyrw-QySlDcLS3TGYh6ACCrRnSrtEfMK
"""

# Commented out IPython magic to ensure Python compatibility.
# Core imports

#Image processing -- may use one or both
import skimage
import cv2
#Python image library is convenient to open images
#from PIL import Image, ImageDraw, ImageFont

#Handling data
import numpy as np
#import scipy
from scipy import ndimage as ndi

#Imports from skimage
from skimage import measure
from skimage import filters
from skimage import morphology
from skimage import io
from skimage.color import rgb2gray
#from skimage.color import gray2rgb
from skimage.filters import threshold_otsu
from skimage import exposure

import matplotlib.pyplot as plt
# %matplotlib inline
#import plotly.express as px

#Imports from sklearn for data clustering
from sklearn.cluster import KMeans

#Imports required to take images through Google Colab
from IPython.display import display, Javascript
#from google.colab.output import eval_js
from base64 import b64decode


#Use this code block to capture images through a webcam.
#Official snippet from Google, with a few modifications to remove excessive imports.

"""
try:
  filename = take_photo_local()
  #print('Saved to {}'.format(filename))
  
  # Show the image which was just taken.
  cap = io.imread(filename)
  io.imshow(cap)
except Exception as err:
  # Errors will be thrown if the user does not have a webcam or if they do not
  # grant the page permission to access it.
  print(str(err))


im = cap.copy()

#First extract the features
features_extracted = FeatureExtractor(im, bound_ratio=5.2, n_filtering=1,
                                      high_exposure=True,
                                      visualize=False,
                                      debugMode=False)
"""

take_snap_local(1)

def take_snap_local(webcam_id=1):
	"""
	From: https://stackoverflow.com/questions/34588464/python-how-to-capture-image-from-webcam-on-click-using-opencv
	Take photos using OpenCV and pass them along to skimage.
	Press SPACE to keep taking pics. Press ESC to stop.

	Inputs: webcam_id - The index of the webcam connection.
			count - The number of photos to capture. 	Outputs: image_rgb - RGB image suitable for skimage.
	"""
	cam = cv2.VideoCapture(webcam_id) #depends on webcam index, search for your own

	cv2.namedWindow("Capture")

	img_counter = 0

	while True():
		ret,frame = cap.read()
		if not ret:
			print("Couldn't take picture.")
			break
		cv2.imshow("Capture",frame)

		k = cv2.waitKey(1)
		if k%256 == 27:
			#ESC pressed
			print("Closing...")
			break
		if k%256 == 32:
			#SPACE pressed
			frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
			feature = FeatureExtractor(frame, bound_ratio=5.2, n_filtering=1,
                                      high_exposure=True,
                                      visualize=False,
                                      debugMode=False)
			img_name = f"feature_frame_{img_counter}.png"
			io.imsave(img_name, feature.flushed_image)
			print(f"Appended to imgs at index {img_counter}")
			img_counter += 1
	
	cam.release()
	cv2.destroyAllWindows()

class Feature: 
  """
  The Feature class, to hold feature properties, and the image of the feature.
  Some attributes may be used as neural network hyperparameters.
  Some attributes are required to find others (passed as None).
  Generate a Feature by first processing an image through FeatureExtractor.

  Dependencies: from skimage import io
                import numpy as np

  Attributes: Read the docstring of FeatureExtractor for more details.
              
              flushed_image:  The color image of the feature bounded in a box,
                              with the background removed.
              
              vector: Feature vector obtained by the neural network.
              
              cluster: Clustering label, used to group objects that are near.
              
              sound: Sound file assigned to the feature. This could be an index
              pointing to the list of sound files, loaded along with training
              data for the neural network. The sound files will have new entries
              if the neural network recognizes the object as "new".
              
              effect_id: Sound effect assigned by the data analysis functions 
              to the feature. Could be a filter, reverb, reverse, etc. 
              Applicable to chords.
              Chords are features clustered by their centroid distances.

  Methods:    imshow(): calls io.imshow() to view the flushed image.
  """
  def __init__(self, index, color, centroid, perimeter, area, orientation, 
               euler_number, compactness, corner_topleft, corner_botright, 
               flushed_image, vector=None, cluster=None, effect_id=None, 
               sound=None):
    self.index = index
    self.color = color
    self.centroid = centroid
    self.perimeter = perimeter
    self.area = area
    self.orientation = orientation
    self.euler_number = euler_number
    self.compactness = compactness
    self.corner_topleft = corner_topleft
    self.corner_botright = corner_botright
    self.flushed_image = flushed_image
    self.vector = vector
    self.cluster = cluster
    self.effect_id = effect_id
    self.sound = sound

  def __str__(self):
    return (
        f'''Feature Number: {self.index},
        Averaged RGB values of feature: {self.color},
        Centroid: {self.centroid},
        Perimeter: {self.perimeter},
        Area: {self.area},
        Orientation [Radians]: {self.orientation},
        Euler Number: {self.euler_number},
        Compactness: {self.compactness},
        Top Left Corner: {self.corner_topleft},
        Bottom Right Corner: {self.corner_botright},
        Cluster: {self.cluster},
        Assigned Effect #ID: {self.effect_id},
        Feature Vector: {self.vector}'''
        
    )
  
  def imshow(self):
    #Returns the feature's "flushed" image, i.e. with zero-background
    #https://github.com/scikit-image/scikit-image/blob/master/skimage/io/_io.py#L139-L159
    return io.imshow(self.flushed_image)

def FeatureExtractor(image, bound_ratio = 4, n_filtering = 2, visualize=False,
                     high_exposure=True,debugMode=False):
  
  """
  Extract from an image, the features on the foreground of the image and their
  attributes. 
  Requires a Feature class to function. 

  Dependencies: import skimage
                import numpy as np
                import scipy.ndimage as ndi
                import skimage.morphology as morphology
                from skimage.filters import threshold_otsu
                from skimage import io
                from skimage import measure
                import matplotlib.pyplot as plt
                from skimage.color import rgb2gray

  Note:
  Can be also used on grayscale images without failure, but the original image
  will be then a grayscale one, when the isolated image is shown; and the color 
  values will be for the grayscale image.

  Inputs: numpy.ndarray (3D RGB image, or 2D grayscale image)
          
          bound_ratio (float): Defines how large the bounding window will be. Explicitly
          defines the ratio of the feature's area to its circumference.
          Recommended value is 4, and the minimum possible value is 3.6. Going
          lower may cause clipping.

          n_filtering(int): The number of iterations for the filtering.

          visualize (bool): True - enable plots for the feature extraction 
          process, to debug in case additional features appear due to 
          inadequate filtering. Defaults to False.

          high_exposure(bool): True - enable gamma correction on the image to
          get a high exposure image. Useful for dark features that don't appear
          after the filtering.

          debugMode (bool): True - Enable debug outputs. Defaults to False.

  Outputs:  feature_idx (list, int) - List of feature label numbers

            colors (list, numpy.ndarray) - List of feature colors in RGB

            centroids (list, float) - List of feature centroid positions 
            on original image.

            perimeters (list, float) - List of feature perimeters
            
            areas (list, float) - List of feature areas
            
            orientations (list, float) - List of feature orientations. 
            The CCW angle with the positive horizontal axis is given in radians.
            
            euler_numbers (list, float) - List of feature Euler Numbers 
            (8-neighbors).
            
            TL_corners (list, tuple, int) - List of feature top left corner
            index positions, on original image. 
            Useful for bounding box generation.
            
            BR_corners (list, tuple, int) - List of feature bottom right corner
            index positions, on original image. 
            Useful for bounding box generation.
  """

  #Load in the image, and get grayscale
  im = image.copy()
  gray = rgb2gray(im)

  #Apply Gamma exposure to grayscale image
  gray_exp = gray.copy()
  if high_exposure == True:
    gray_exp = exposure.adjust_gamma(gray_exp,gamma=0.35, gain=1)

  #Binarization, using Otsu's thresholding method
  thresh = threshold_otsu(gray_exp)

  binary = gray_exp > thresh 

  #Salt and pepper noise is to be removed inside the objects

  fill_objects = binary.copy()

  for _ in range(n_filtering):
    fill_objects = morphology.binary_erosion(fill_objects)
    fill_objects = ndi.binary_fill_holes(fill_objects)
    fill_objects = morphology.binary_opening(fill_objects)
    fill_objects = morphology.binary_closing(fill_objects)
    fill_objects = morphology.binary_dilation(fill_objects)
  
  labels = measure.label(fill_objects, background = 0)
  if debugMode == True:
    print(f"The number of features recognized:{labels.max()}") #should be equal to the number of objects on the picture

  #Using connected component labeling tools of skimage
  props = measure.regionprops(labels)
  
  features = []

  feature_idx = []
  colors = []
  centroids = []
  perimeters = []
  areas = []
  orientations = []
  euler_numbers = []
  compactnesses = []
  TL_corners = []
  BR_corners = []

  for label_pick in range(1,labels.max()+1):
    centroid = props[label_pick-1].centroid
    perimeter = props[label_pick-1].perimeter
    area = props[label_pick-1].area
    orientation = props[label_pick-1].orientation
    euler_number = props[label_pick-1].euler_number #8-neighbors
    compactness = (perimeter**2/area)/(4*np.pi) #defined in class

    #Get a trimmed off image
    feature = np.zeros_like(labels)
    feature = np.where((labels==label_pick),labels,0)
    trimmed = feature.copy()

    #initialize boundaries
    bound_top = 0
    bound_bot = feature.shape[0]
    bound_left = 0
    bound_right = feature.shape[1]

    #use bounded frames derived from centroid position, area, and perimeter data
    #quite effective in bounding anything at all - details in the formulation below
    bound = (area/perimeter)*bound_ratio
    center = tuple([int(centroid[0]),int(centroid[1])])

    if center[0]-bound > 0:
      bound_top = int(center[0]-bound)
    else:
      bound_top = 0

    if center[0]+bound < im.shape[0]:
      bound_bot = int(center[0]+bound)
    else:
      bound_bot = im.shape[0]

    if center[1]-bound > 0:
      bound_left = int(center[1]-bound)
    else:
      bound_left = 0

    if center[1]+bound < im.shape[1]:
      bound_right = int(center[1]+bound)
    else:
      bound_right = im.shape[1]

    trimmed = feature[bound_top:bound_bot,bound_left:bound_right]
    #plt.imshow(trimmed, cmap="nipy_spectral")

    im_trimmed = im[bound_top:bound_bot,bound_left:bound_right].copy()
    im_flushed = im_trimmed.copy()

    im_flushed[:,:,0] = np.where(trimmed==label_pick, im_trimmed[:,:,0], 0)
    im_flushed[:,:,1] = np.where(trimmed==label_pick, im_trimmed[:,:,1], 0)
    im_flushed[:,:,2] = np.where(trimmed==label_pick, im_trimmed[:,:,2], 0)

    feature_color = np.zeros((1,3))

    red_flushed = im_flushed[:,:,0]
    green_flushed = im_flushed[:,:,1]
    blue_flushed = im_flushed[:,:,2]

    feature_color[:,0] = red_flushed[red_flushed.nonzero()].mean()
    feature_color[:,1] = green_flushed[green_flushed.nonzero()].mean()
    feature_color[:,2] = blue_flushed[blue_flushed.nonzero()].mean()

    if debugMode == True:
      print(f"Feature Number: {label_pick}")
      print(f"Averaged RGB values of feature: {feature_color}")
      print(f"Centroid: {centroid}")
      print(f"Perimeter: {perimeter}")
      print(f"Area: {area}")
      print(f"Orientation [Radians]: {orientation}")
      print(f"Euler Number: {euler_number}")
      print(f"Compactness: {compactness}")
      print(f"Top Left Corner: {(bound_top,bound_left)}")
      print(f"Bottom Right Corner: {(bound_bot,bound_right)}")
    """
    if isClass == False:
      feature_idx.append(label_pick)
      colors.append(feature_color)
      centroids.append(centroid)
      perimeters.append(perimeter)
      areas.append(area)
      orientations.append(orientation)
      euler_numbers.append(euler_number)
      compactnesses.append(compactness)
      TL_corners.append((bound_top,bound_left))
      BR_corners.append((bound_bot,bound_right)) 
    
    if isClass == True:
    """
    features.append(Feature(label_pick,feature_color,centroid,perimeter,area,
                            orientation,euler_number,compactness,
                            (bound_top,bound_left),
                            (bound_bot,bound_right),im_flushed))


    if visualize == True:
      plt.figure(figsize=(18, 7))
      plt.subplot(151, title="Denoised Binary")
      plt.imshow(fill_objects, cmap='gray')
      plt.axis('off')
      plt.subplot(152, title= "All labels")
      plt.imshow(labels, cmap='nipy_spectral')
      plt.axis('off')
      plt.subplot(153, title= "Grayscale image")
      plt.imshow(gray, cmap='gray')
      plt.axis('off')
      plt.subplot(154, title= "Isolated image")
      plt.imshow(np.where((labels==label_pick),labels,0), cmap="nipy_spectral")
      plt.axis('off')
      plt.subplot(155, title= f"Flushed feature image: {label_pick}")
      plt.imshow(im_flushed, cmap="nipy_spectral")
      plt.axis('off')
  """
  if isClass == False:
    return (feature_idx, colors, centroids, perimeters, areas, orientations, 
          euler_numbers, compactnesses, TL_corners, BR_corners)
  elif isClass == True:
  """
  return features

def ImageReconstruction(img,features,binary=False):
  canvas = np.zeros_like(img)

  for f in features:
    canvas[f.corner_topleft[0]:f.corner_botright[0],f.corner_topleft[1]:
           f.corner_botright[1]] += f.flushed_image.copy()
  
  if binary == True:
    canvas_gray = rgb2gray(canvas)
    thresh_canvas = threshold_otsu(canvas_gray)
    binary = canvas_gray > thresh_canvas
    return binary

  return canvas

def PrincipalOrientation(features, debugMode = False):
  """
  A simple function that takes in a cluster of three features from an image to 
  fit a line among their centroids, using Singular Value Decomposition. 
  
  Returns the "axis of principal orientation" that this line is the closest to.
  The principal orientation axes are defined to be the:
    0: horizontal axis
    1: left diagonal axis
    2: vertical axis
    3: right diagonal axis

  Dependencies: Feature class
                import numpy as np

  Inputs: features (list, Feature class) -- A list of 3 features from the image.
          debugMode (bool) -- Toggle to display debug information.

  Outputs: dex -- The index of the nearest principal axis, among 4.
  """
  """
  #Grab the centroid positions from the features
  centroids = np.zeros((len(features),2))
  for idx in range(len(features)):
    centroids[idx][0] = features[idx].centroid[0]
    centroids[idx][1] = features[idx].centroid[1]
  #"""
  #"""
  #Centroids seem to be not very accurate. How about frame corners?
  centroids = np.zeros((len(features),2))
  for idx in range(len(features)):
    centroids[idx][0] = (features[idx].corner_topleft[0]+features[idx].corner_botright[0])/2
    centroids[idx][1] = (features[idx].corner_topleft[1]+features[idx].corner_botright[1])/2
  #"""
  #Defining the principal axes. 
  #Since fitted lines won't have a sense of direction,
  #most values will be paired together for the labeling
  principals = [0,45,90,135,180,225,270,315]

  u, s, vt = np.linalg.svd(centroids, full_matrices=True)
  if debugMode == True:
    print(f"U Matrix:\n {u}")
    print(f"Singular Values:\n {s}")
    print(f"VT Matrix:\n {vt}")

  ang=np.abs(np.arctan2(vt[0][1],vt[0][0])*180/np.pi)
  if debugMode == True:
    print(f"Directed angle of best fit line [degrees]: {ang}")
  
  if debugMode == True:
    print(f"Nearest principal axis angle: {principals[(np.abs(principals-ang)).argmin()]}")
  dex = (np.abs(principals-ang)).argmin()
  if dex >= 4:
    dex -= 4
  
  return dex

def DistanceEuclidean(features):
  """
  Returns the Euclidean distance among the centroids of
  given two features. Only returns the distance between
  the first two list items.
  
  Input: features (list, Feature class)
  Output: distance_euclidean (float)
  """
  #obtain centroid indices by list comprehension
  c = [f.centroid for f in features]
  #compute Euclidean distance
  distance_euclidean = ((c[0][0]-c[1][0])**2+(c[0][1]-c[1][1])**2)**0.5

  return distance_euclidean

def FeatureGeoClustering(features_extracted,tolerance=275,n_iterations=3,debugMode=False):
  """
  Dependencies: def distanceEuclidean
                class Feature
                import numpy as np
                from sklearn.cluster import KMeans

  Inputs: features_extracted (list of Feature class instances) - features to be clustered
          tolerance (float) - maximum centroid distance allowed within a cluster
          n_iterations (int) - number of iterations to check centroid distances
          debugMode (bool) - toggle to enable debug information

  Outputs: fcl (list of Feature class instances) - updated features with cluster labels

  Note: There will be a cluster label (label value -1) that is 
  for anything that cannot be clustered for the given conditions.
  This cluster of discarded features will be treated as individual ones.
  Validated clusters of features will have positive label values and can be
  grouped together by calling Feature.cluster
  """

  #Dividing the number of features by three and taking ceiling to generate the  
  #number of maximum possible valid clusters
  n_clusters = np.ceil(len(features_extracted)/3).astype(int)
  if debugMode == True:
    print(f"Expected maximum number of clusters:{n_clusters}")

  kmeans = KMeans(n_clusters = n_clusters).fit([f.centroid for f in features_extracted])

  #Got the first guesses on the clusters, using KMeans!
  for idx in range(len(features_extracted)):
    features_extracted[idx].cluster = kmeans.labels_[idx]
  if debugMode == True:
    print(f"List of initial cluster labels:{[f.cluster for f in features_extracted]}")
  
  #creating shorthand name for convenience
  fcl = features_extracted.copy()

  #Now it is required to "detach" some of these items from the clusters
  #may need to run this multiple times, just like KMeans
  for _ in range(n_iterations):
    
    for i in range(n_clusters):
      #iterate through cluster labels that KMeans printed out

      #store elements of this cluster label in this list
      cls = []
      #go through the feature list to store them in cls
      for f in fcl:
        if f.cluster == i:
          cls.append(f)
      
      #now cleaning up the clustering according to the distances

      #if there are more than 2 elements, check distances
      if len(cls) > 2:
        for j in range(len(cls)): #compare this element with others 

          off_range = True #unless it is proved that they are close, they are not
          
          for k in range(len(cls)): 
            if j!=k: #don't compare it with itself
              dist = DistanceEuclidean([cls[j],cls[k]]) #get Euclidean distance
              if debugMode == True:
                print(f"Comparing: {cls[j].index} with {cls[k].index}. Dist: {dist}")
              if dist < tolerance: 
                off_range = False #if within tolerance, it's not off-range
          
          #if it is off-range, it doesn't belong to this cluster
          if off_range == True:
            #group it with the other discarded items
            cls[j].cluster = -1

          if debugMode == True:
            print(f"Index: {cls[j].index}, off_range: {off_range}")
      
      else: #if less than 3 elements, discard entire cluster proposition
        for elem in cls: 
          elem.cluster = -1

    if debugMode == True:
      print([f.cluster for f in fcl])

  return fcl

def FeatureLineFitting(features_clustered,debugID=[0,0],debugMode=False):
  """
  A function to pass feature lists with clustering information, to obtain
  the closest diagonal, horizontal or vertical line that passes nearest to the
  best fit lines of each cluster.

  This function must be called after using GeoClustering.

  Inputs: features_clustered (list of Feature class instances)
  Outputs: features_analyzed (list of Feature class instances)
  """

  #copying over the clustered features list
  features_analyzed = features_clustered.copy()

  #the end condition for the for loop:
  n_cluster_iter = max([f.cluster for f in features_clustered])+1

  for i in range(n_cluster_iter):
    #pick all cluster elements of the given non-negative label
    cluster_elems = [f for f in features_analyzed if f.cluster == i]
    #use data analysis to fit a line, and select closest line representing 
    #the effect_id of this "chord"
    effect_id = PrincipalOrientation(cluster_elems,debugMode=(i in debugID))
    #assign this effect_id to all elements
    for elem in cluster_elems:
      elem.effect_id = effect_id
  
    if (debugMode == True):
      print(f"Assigning Cluster: {i}")
      print(f"Effect #ID's that are assigned: {effect_id}")
      #features_analyzed[i].imshow() #don't want to flood the screen with images
    
  return features_analyzed